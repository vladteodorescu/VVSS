package biblioteca.repository.repo;


import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;

import java.io.*;
import java.net.URL;
import java.util.*;

public class CartiRepo implements CartiRepoInterface {
	
	private String file = "cartiBD.dat";
	private List<Carte> carti;
	
	public CartiRepo(){
		URL location = CartiRepo.class.getProtectionDomain().getCodeSource().getLocation();
        System.out.println(location.getFile());
        carti = new ArrayList<Carte>();
		carti = getCarti();
	}
	
	@Override
	public void adaugaCarte(Carte carte) {
		BufferedWriter bufferedWriter = null;
		try {
			bufferedWriter = new BufferedWriter(new FileWriter(file,true));
			bufferedWriter.write(carte.toString());
			bufferedWriter.newLine();
			
			bufferedWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		carti = getCarti();
	}

	@Override
	public List<Carte> getCarti() {
		List<Carte> carteList = new ArrayList<Carte>();
		BufferedReader bufferedReader = null;
		try {
			bufferedReader = new BufferedReader(new FileReader(file));
			String line = null;
			while((line=bufferedReader.readLine())!=null){
				carteList.add(Carte.getCarteFromString(line));
			}
			
			bufferedReader.close();
		} catch (FileNotFoundException exception) {
			exception.printStackTrace();
		} catch (IOException IOexception) {
			IOexception.printStackTrace();
		}
		
		return carteList;
	}

	@Override
	public void modificaCarte(Carte nou, Carte vechi) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void stergeCarte(Carte carte) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<Carte> cautaCarte(String referent) {
		List<Carte> cartiGasite = new ArrayList<Carte>();
		int index=0;
		while (index<carti.size()){
			boolean flag = false;
			List<String> referentiList = carti.get(index).getReferenti();
			int referentiListIndex = 0;
			while(referentiListIndex<referentiList.size()){
				if(referentiList.get(referentiListIndex).toLowerCase().contains(referent.toLowerCase())){
					flag = true;
					break;
				}
				referentiListIndex++;
			}
			if(flag){
				cartiGasite.add(carti.get(index));
			}
			index++;
		}
		return cartiGasite;
	}

	@Override
	public List<Carte> getCartiOrdonateDinAnul(Integer an) {
		List<Carte> cartiDinAnul = new ArrayList<Carte>();
		for(Carte c:carti){
			if(c.getAnAparitie().equals(an)){
				cartiDinAnul.add(c);
			}
		}
		
		Collections.sort(cartiDinAnul,new Comparator<Carte>(){

			@Override
			public int compare(Carte a, Carte b) {
				if(a.getTitlu().compareTo(b.getTitlu())==0){
					return a.getReferenti().get(0).compareTo(b.getReferenti().get(0));
				}

				return a.getTitlu().compareTo(b.getTitlu());
			}
		
		});
		
		return cartiDinAnul;
	}

}
