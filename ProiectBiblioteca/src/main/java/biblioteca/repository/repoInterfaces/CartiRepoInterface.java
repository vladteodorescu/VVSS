package biblioteca.repository.repoInterfaces;


import biblioteca.model.Carte;

import java.util.List;

public interface CartiRepoInterface {
	void adaugaCarte(Carte carte);
	void modificaCarte(Carte nou, Carte vechi);
	void stergeCarte(Carte carte);
	List<Carte> cautaCarte(String referent);
	List<Carte> getCarti();
	List<Carte> getCartiOrdonateDinAnul(Integer an);
}
