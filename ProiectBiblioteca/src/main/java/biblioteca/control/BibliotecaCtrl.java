package biblioteca.control;


import biblioteca.model.Carte;
import biblioteca.repository.repoInterfaces.CartiRepoInterface;
import biblioteca.util.ValidatorCarte;

import java.util.List;

public class BibliotecaCtrl {


	private CartiRepoInterface cartiRepoInterface;
	
	public BibliotecaCtrl(CartiRepoInterface cartiRepoInterface){
		this.cartiRepoInterface = cartiRepoInterface;
	}
	
	public void adaugaCarte(Carte carte) throws Exception{
		ValidatorCarte.validateCarte(carte);
		cartiRepoInterface.adaugaCarte(carte);
	}
	
	public void modificaCarte(Carte nou, Carte vechi) throws Exception{
		cartiRepoInterface.modificaCarte(nou, vechi);
	}
	
	public void stergeCarte(Carte carte) throws Exception{
		cartiRepoInterface.stergeCarte(carte);
	}

	public List<Carte> cautaCarte(String autor) throws Exception{
		ValidatorCarte.isStringOK(autor);
		return cartiRepoInterface.cautaCarte(autor);
	}
	
	public List<Carte> getCarti() throws Exception{
		return cartiRepoInterface.getCarti();
	}
	
	public List<Carte> getCartiOrdonateDinAnul(String an) throws Exception{
		if(!ValidatorCarte.isNumber(an))
			throw new Exception("Nu e numar!");
		return cartiRepoInterface.getCartiOrdonateDinAnul(Integer.parseInt(an));
	}
	
	
}
